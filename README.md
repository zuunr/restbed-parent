# restbed-parent

![Apache License](https://img.shields.io/static/v1.svg?label=License&message=Apache-2.0&color=%230af)
[![Maven Central](https://img.shields.io/maven-central/v/com.zuunr.restbed/restbed-parent.svg?label=Maven%20Central&color=%230af)](https://search.maven.org/search?q=a:restbed-parent)
[![Snyk](https://snyk-widget.herokuapp.com/badge/mvn/com.zuunr.restbed/restbed-parent/badge.svg)](https://snyk.io/vuln/maven:com.zuunr.restbed%3Arestbed-parent)

This is the parent pom for Restbed modules. It includes project wide plugin and dependency management.

## Supported tags

* [`1.0.3-1b23914`, (*1b23914/pom.xml*)](https://bitbucket.org/zuunr/restbed-parent/src/1b23914/pom.xml)
* [`1.0.2-181f436`, (*181f436/pom.xml*)](https://bitbucket.org/zuunr/restbed-parent/src/181f436/pom.xml)
* [`1.0.1-b7a034f`, (*b7a034f/pom.xml*)](https://bitbucket.org/zuunr/restbed-parent/src/b7a034f/pom.xml)
* [`1.0.0-30b4dfa`, (*30b4dfa/pom.xml*)](https://bitbucket.org/zuunr/restbed-parent/src/30b4dfa/pom.xml)

## Usage

Include this pom as the parent pom of your Restbed module:

```xml
<parent>
    <groupId>com.zuunr.restbed</groupId>
    <artifactId>restbed-parent</artifactId>
    <version>1.0.3-1b23914</version>
</parent>
```
